#![feature(test)]
use std::hint::black_box;
use std::io::Cursor;
use utf8_bufread::BufRead;

fn main() {
    let mut r = Cursor::new("");
    let s1 = r.read_str().unwrap();
    let _s2 = r.read_str().unwrap();
    black_box(s1);
}
