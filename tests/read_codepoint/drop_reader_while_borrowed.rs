use std::io::Cursor;
use utf8_bufread::BufRead;

fn main() {
    let _s = {
        let mut r = Cursor::new("");
        r.read_codepoint().unwrap()
    };
}
